from django.test import TransactionTestCase


class AutoCreateUserProfileTest(TransactionTestCase):
    def test_create_user_happy_flow(self):
        from django.contrib.auth import get_user_model

        user_cls = get_user_model()
        user = user_cls(email='example@example.com',
                        first_name='Dr. Examplović', last_name="Hello")
        user.save()

        self.assertEqual('Dr. Examplović Hello', user.full_name)

        return user
