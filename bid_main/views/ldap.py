# SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-LicenseRef: GPL-3.0-or-later

from django.core.exceptions import ValidationError
from django.http import HttpRequest, HttpResponseRedirect
from django.views.generic.edit import FormView
from ldap3 import Server, Connection
from datetime import date
from django.contrib import messages
from ldap3.core.exceptions import LDAPException

from .. import forms, models


class LdapAuthentication(FormView):
    """
    View responsible to the LDAP import of account.
    """
    form_class = forms.LDAPAuthenticationForm
    template_name = 'bid_main/ldap/auth.html'
    success_url = '/'

    def form_valid(self, form: forms.LDAPAuthenticationForm) -> HttpResponseRedirect:
        server = Server('ldap://localhost:389')

        try:
            conn = Connection(server, 'uid=' + form.cleaned_data['username'] + ',ou=people,dc=kde,dc=org',
                              form.cleaned_data['password'], auto_bind=True)
        except LDAPException:
            form.add_error('password', ValidationError('Wrong password or username'))
            return super().form_invalid(form)

        if conn.search('dc=kde,dc=org', '(uid=' + form.cleaned_data['username'] + ')',
                       attributes=['sn', 'mail', 'givenName', 'uid', 'groupMember', 'secondaryMail']):
            ldap_user = conn.entries[0]

            if models.User.objects.filter(nickname=form.cleaned_data['username'],
                                          email=ldap_user.mail.values[0]).count() > 0:
                user = models.User.objects.get(nickname=form.cleaned_data['username'],
                                               email=ldap_user.mail.values[0])
            else:
                user = models.User.objects.create_user(ldap_user.mail.values[0], form.cleaned_data['password'])

            user.first_name = ldap_user.givenName.values[0]
            user.last_name = ldap_user.sn.values[0]
            user.nickname = ldap_user.uid.values[0]
            user.confirmed_email_at = date.today()
            user.save()

            # import secondary emails
            for mail in ldap_user.secondaryMail.values:
                models.SecondaryEmail.objects.create(email=mail, user=user, token=None)

            # import groups memberships
            for group in ldap_user.groupMember.values:
                roles = models.Role.objects.filter(name=group)

                if group == 'sysadmins':
                    user.is_staff = True
                    user.is_superuser = True
                    user.save()

                if len(roles) < 1:
                    role = models.Role.objects.create(name=group, description='Fill Description',
                                                      is_active=True, is_badge=False)
                else:
                    role = roles[0]
                user.roles.add(role)

            user.save()
            conn.unbind()
            messages.success(self.request, 'Your identity account was imported with success.')
            return super().form_valid(form)
